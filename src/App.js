import './App.css';
import AppNavbar from './components/AppNavbar';

import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Profile from './pages/Profile';
import NotFound from './pages/NotFound';
import Products from './pages/Products';
import ProductItem from './pages/ProductItem';
import SetAdmin from './pages/SetAdmin';
import Orders from './pages/Orders.js'

import {Container} from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext';

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/api/users/details`, {
      method: 'POST',
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          id: localStorage.getItem('userId')
        })
    })
    .then(response => response.json())
    .then(result => {
      if (typeof result._id !== 'undefined') {
        // This ensures that the 'user' state will still have its values re-assigned
        setUser({
          id: result._id,
          isAdmin: result.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null,
        })
      }
    })
  }, [])


  return (
    <>
    <div className="page-wrapper">
      <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
          <AppNavbar/>
          <Container>
            <Routes>
              <Route path='/' element = {<Home/>}/>
              <Route path='/products' element = {<Products/>}/>
              <Route path='/products/:productId' element = {<ProductItem/>}/>
              <Route path='/register' element = {<Register/>}/>
              <Route path='/login' element = {<Login/>}/>
              <Route path='/logout' element = {<Logout/>}/>
              <Route path='/profile' element = {<Profile/>}/>
              <Route path='/setadmin' element = {<SetAdmin/>}/>
              <Route path='/orders' element = {<Orders/>}/>
              <Route path='*' element={<NotFound/>} />
            </Routes>
          </Container>
        </Router>
      </UserProvider>
    </div>
    </>
  );
}

export default App;
