import {Row, Col, Card, Image} from 'react-bootstrap';

export default function Highlights(){
    return(
        <Row className="pb-5">
            <Col xs={12} md={4} className = "mb-4 mb-md-0">
                <Card className="cardHighlight p-3 card-color">
                    <Card.Body>
                        <div className='text-center mb-2'>
                            <Image src='https://i.imgur.com/H3W97qk.jpg' className='img-fluid w-75'></Image>
                        </div>
                        <Card.Title>
                            <h2 className="text-center">About Elektro Knicks</h2>
                        </Card.Title>
                        <Card.Text>
                            Your one-stop destination for nostalgia-driven tech enthusiasts. Explore a curated collection of vintage treasures! Rediscover the magic of past times with our handpicked selection of tech relics that have stood the test of time.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4} className = "mb-4 mb-md-0">
                <Card className="cardHighlight p-3 card-color">
                    <Card.Body>
                        <div className='text-center mb-2'>
                            <Image src='https://i.imgur.com/ahO0uZN.jpg' className='img-fluid w-75'></Image>
                        </div>
                        <Card.Title>
                            <h2 className="text-center">Past Technologies</h2>
                        </Card.Title>
                        <Card.Text>
                            We celebrate the innovations of bygone eras, offering a glimpse into the evolution of technology. From early gaming joys found in Gameboys to the nostalgic charm of Polaroid cameras, our curated selection showcases the enduring appeal of these past technologies.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4} className = "mb-4 mb-md-0">
                <Card className="cardHighlight p-3 card-color">
                    <Card.Body>
                        <div className='text-center mb-2'>
                            <Image src='https://i.imgur.com/IOffbfh.jpg' className='img-fluid w-75'></Image>
                        </div>
                        <Card.Title>
                            <h2 className="text-center">Preserve History</h2>
                        </Card.Title>
                        <Card.Text>
                            We're not just selling gadgets; we're preserving history. Each item in our collection is a testament to the ingenuity of its time. Explore our selection of vintage tech and take a step back in time. Join us in safeguarding the legacy of these remarkable innovations.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
}