import { Card } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default function ProductCard({product}){

	// Destructuring contents of 'product'
	const {_id, name, description, price} = product;

    return (
        <Card className = "my-3 card-color">
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text> 

				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>PHP {price}</Card.Text>

			    <Link className = 'btn btn-primary' to = {`/products/${_id}`}>View Details</Link>
			</Card.Body>
		</Card>
    )
}

// PropTypes is used for validating the data from the props
ProductCard.propTypes = {
	product: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}