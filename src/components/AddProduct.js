import { useState } from 'react';
import { Form, Button, Modal } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AddProduct({fetchProducts}){

    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(1);

    const [createProductModal, setCreateProductModal] = useState(false);

    const openCreateProductModal = () => {
        setCreateProductModal(true);
    }

    const closeCreateProductModal = () => {
        setCreateProductModal(false);
        setName('');
        setDescription('');
        setPrice('');
    }

    const addProduct = (event) => {
        event.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/api/products/register`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        })
        .then(response => response.json())
        .then(result => {
            if (result){
                Swal.fire({
                    title: 'New Product Added!',
                    icon: 'success'
                })

                // Clear Field Form
                setName('')
				setDescription('')
				setPrice(1)
                fetchProducts();
            } else {
                Swal.fire({
                    title: 'Something went wrong..',
                    text: 'Product creation unsuccessful..',
                    icon: 'warning'
                })
            }
        })
    }

    return(
        <>
            <Button variant = "info" size = "md" className='m-5 justify-content-center button-center' onClick = {() => openCreateProductModal()}>Add Product</Button>

            <Modal show = {createProductModal} onHide = {closeCreateProductModal}>
                <Form onSubmit = {event => addProduct(event)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Add Product</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>

                        <Form.Group controlId="productName">
                            <Form.Label>Name</Form.Label>
                            <Form.Control 
                            type="text"
                            value = {name} 
                            onChange = {event => setName(event.target.value)}
                            required/>
                        </Form.Group>

                        <Form.Group controlId="productDescription">
                            <Form.Label>Description</Form.Label>
                            <Form.Control 
                            type="text" 
                            value = {description} 
                            onChange = {event => setDescription(event.target.value)}
                            required/>
                        </Form.Group>

                        <Form.Group controlId="productPrice">
                            <Form.Label>Price</Form.Label>
                            <Form.Control 
                            type="number" 
                            value = {price} 
                            onChange={event => {
                                const newPrice = parseInt(event.target.value);
                                if (!isNaN(newPrice) && newPrice >= 1) {
                                    setPrice(newPrice);
                                } else {
                                    setPrice(1);
                                }
                            }}
                            required/>
                        </Form.Group>

                    </Modal.Body>

                    <Modal.Footer>
                        <Button variant='secondary' onClick = {closeCreateProductModal}>Close</Button>
                        <Button variant='success' type='submit'>Submit</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    )
}