import {Navbar, Nav, Container} from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import { useContext } from 'react';
import UserContext from '../UserContext'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faWalkieTalkie } from '@fortawesome/free-solid-svg-icons';

export default function AppNavbar(){

    const { user } = useContext(UserContext);

    return (
        <Navbar className="navbar-bg" expand="lg">
            <Container fluid>
                <Navbar.Brand as = {Link} to = '/' className = "mx-md-5 bold-text">
                    <FontAwesomeIcon icon={faWalkieTalkie}/> Elektro Knicks
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ms-auto">
                        <Nav.Link as = {NavLink} to = '/'>Home</Nav.Link>
                        {
                            (user.id !== null) ? 
                                user.isAdmin ?
                                <>
                                    <Nav.Link as = {NavLink} to ='/products'>Products</Nav.Link>
                                    <Nav.Link as = {NavLink} to ='/orders'>User Orders</Nav.Link>
                                    <Nav.Link as = {NavLink} to ='/setadmin'>Grant Admin</Nav.Link>
                                    <Nav.Link as = {NavLink} to = '/profile'>Profile</Nav.Link>
                                    <Nav.Link as = {NavLink} to ='/logout'>Logout</Nav.Link>
                                </>
                                :
                                <>
                                    <Nav.Link as = {NavLink} to = '/products'>Products</Nav.Link>
                                    <Nav.Link as = {NavLink} to = '/orders'>Orders</Nav.Link>
                                    <Nav.Link as = {NavLink} to = '/profile'>Profile</Nav.Link>
                                    <Nav.Link as = {NavLink} to = '/logout'>Logout</Nav.Link>
                                </> 
                                :
                                <>
                                    <Nav.Link as = {NavLink} to = '/products'>Products</Nav.Link>
                                    <Nav.Link as = {NavLink} to = '/register'>Register</Nav.Link>
                                    <Nav.Link as = {NavLink} to = '/login'>Login</Nav.Link>
                                </>
                        }
                        
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}