import { Button, Modal, Form } from 'react-bootstrap';
import { useState, useContext } from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function ChangePassword() {

    const {user} = useContext(UserContext)
    const [password, setPassword] = useState('')
    const [newPassword, setNewPassword] = useState('')
    const [newPasswordRepeat, setNewPasswordRepeat] = useState('')

    const [showUpdateModal, setUpdateModal] = useState(false);

    const openEditModal = () => {
        setUpdateModal(true);
    }

    const closeUpdateModal = () => {
        setPassword('')
        setNewPassword('')
        setNewPasswordRepeat('')
        setUpdateModal(false)
    }

    const editPassword = (event) => {
        event.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/api/users/changePassword`, {
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id: user.id,
                currentPass: password,
                newPass: newPassword,
                newPassRepeat: newPasswordRepeat
            })
        })
        .then(response => response.json())
        .then(result => {
            if(result.message === 'Password changed successfully!') {
                Swal.fire({
                    title: "Password updated!",
                    text: result.message,
                    icon: 'success'
                })
                closeUpdateModal();
            } else {
                Swal.fire({
                    title: "Something went wrong.",
                    text: result.message,
                    icon: 'error'
                })
                closeUpdateModal();
            }
        })
        
    }

    return(
        <>
            <Button variant = "primary" size = "md" onClick = {() => openEditModal()}>Change Password</Button>

            <Modal show = {showUpdateModal} onHide = {closeUpdateModal}>
                <Form onSubmit = {event => editPassword(event)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Update Password</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>

                        <Form.Group controlId="userPassword">
                            <Form.Label>Current Password</Form.Label>
                            <Form.Control 
                            type="password"
                            value = {password} 
                            onChange = {event => setPassword(event.target.value)}
                            required/>
                        </Form.Group>

                        <Form.Group controlId="userNewPassword">
                            <Form.Label>New Password</Form.Label>
                            <Form.Control 
                            type="password" 
                            value = {newPassword} 
                            onChange = {event => setNewPassword(event.target.value)}
                            required/>
                        </Form.Group>

                        <Form.Group controlId="userNewPasswordRepeat">
                            <Form.Label>Repeat New Password</Form.Label>
                            <Form.Control 
                            type="password" 
                            value = {newPasswordRepeat} 
                            onChange = {event => setNewPasswordRepeat(event.target.value)}
                            required/>
                        </Form.Group>

                    </Modal.Body>

                    <Modal.Footer>
                        <Button variant='secondary' onClick = {closeUpdateModal}>Close</Button>
                        <Button variant='success' type='submit'>Submit</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    )
}