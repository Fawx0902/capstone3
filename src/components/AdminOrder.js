import { useState, useEffect } from 'react';
import { Table } from 'react-bootstrap';
import { format } from 'date-fns';

export default function UserView({ ordersData }) {
    const [orders, setOrders] = useState([]);
    
    useEffect(() => {
        const user_orders = ordersData.map(order => {
            const date = new Date(order.purchasedOn);
            const formattedDate = format(date, 'yyyy-MM-dd HH:mm:ss');
            const productDetails = order.products.map(product => (
                <>
                    <td>
                        {product.productId}
                    </td>
                    <td>
                    {product.productName} - PHP {product.price} 
                    </td>

                    <td>
                        {product.quantity}x
                    </td>
                </>          
            ));

            return (
                <tr key={order._id}>
                    <td>{order._id}</td>
                    <td>{order.userId}</td>
                    <td>{order.email}</td>
                    <td>{formattedDate}</td>
                    {productDetails}
                    <td>{order.totalAmount}</td>
                </tr>
            )
        })

        setOrders(user_orders);
    }, [ordersData])
    
    return (
        <>
            <Table striped bordered hover responsive className="table-format">
                <thead>
                    <tr>
                        <th>Order ID</th>
                        <th>User ID</th>
                        <th>User Email</th>
                        <th>Date of Purchase</th>
                        <th>Product ID</th>
                        <th>Product</th>
                        <th>Quantity</th>
                        <th>Total Amount</th>
                    </tr>
                </thead>
                <tbody>
                {
                    (orders.length === 0) ?
                        <tr>
                            <td colSpan="7">No Orders Found in Database</td>
                        </tr>
                    :
                        orders
                }
                </tbody>
            </Table>
        </>
    )
}
