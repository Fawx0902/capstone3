export default function LoadingScreen() {
    return (
        <div className="loading">
            <div className="spinner-container">
                <div className="spinner"></div>
            </div>
            <p className="p-3">Loading...</p>
        </div>
    )
}