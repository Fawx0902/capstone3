import { useState, useEffect } from 'react';
import { Table } from 'react-bootstrap';
import { format } from 'date-fns';

export default function UserView({ ordersData, userId }) {
    const [orders, setOrders] = useState([]);
    
    useEffect(() => {
        const user_orders = ordersData.map(order => {
            if (order.userId === userId) {
                const date = new Date(order.purchasedOn);
                const formattedDate = format(date, 'yyyy-MM-dd HH:mm:ss');
                const productDetails = order.products.map(product => (
                    <>
                        <td>
                        {product.productName} - PHP {product.price} 
                        </td>

                        <td>
                            {product.quantity}x
                        </td>
                    </>          
                ));

                return (
                    <tr key={order._id}>
                        <td>{order._id}</td>
                        <td>{formattedDate}</td>
                        {productDetails}
                        <td>{order.totalAmount}</td>
                    </tr>
                )
            } else {
                return null
            }
        })

        setOrders(user_orders);
    }, [ordersData, userId])
    
    return (
        <>
            <Table striped bordered hover responsive className="table-format">
                <thead>
                    <tr>
                        <th>Order ID</th>
                        <th>Date of Purchase</th>
                        <th>Product</th>
                        <th>Quantity</th>
                        <th>Total Amount</th>
                    </tr>
                </thead>
                <tbody>
                {
                    (orders.length === 0) ?
                        <tr>
                            <td colSpan="5">No Orders Found in Database</td>
                        </tr>
                    :
                        orders
                }
                </tbody>
            </Table>
        </>
    )
}
