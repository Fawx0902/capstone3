import { Button, Modal, Form } from 'react-bootstrap';
import { useState } from 'react';
import Swal from 'sweetalert2';

export default function EditProduct({product_id, product_name, fetchProducts}) {
    // This state contains the ID of the course that is selected
    const [productId, setproductId] = useState('');

    // Form States
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');

    // Model Activation
    const [showEditModal, setShowEditModal] = useState(false);


    const openEditModal = (productId) => {
        fetch(`${process.env.REACT_APP_API_URL}/api/products/${productId}`)
        .then(response => response.json())
        .then(result => {
            //Pre-populate the form input fields with data from the API
            setproductId(result._id)
            setName(result.name)
            setDescription(result.description)
            setPrice(result.price)
        })

        setShowEditModal(true);
    }

    const closeEditModal = () => {
        setShowEditModal(false)
        setName('')
        setDescription('')
        setPrice('')
    }

    const editProduct = (event, productId) => {
        event.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/api/products/${productId}`, {
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        })
        .then(response => response.json())
        .then(result => {
            if(result) {
                Swal.fire({
                    title: "Product Updated!",
                    text: `Product ${product_name} successfully updated.`,
                    icon: 'success'
                })
                fetchProducts();
                closeEditModal();
            } else {
                Swal.fire({
                    title: "Something went wrong.",
                    text: "Please try again.",
                    icon: 'error'
                })
                fetchProducts();
                closeEditModal();
            }
        })
        
    }

    return(
        <>
            <Button variant = "primary" size = "sm" onClick = {() => openEditModal(product_id)}>Edit</Button>

            <Modal show = {showEditModal} onHide = {closeEditModal}>
                <Form onSubmit = {event => editProduct(event, productId)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Edit Course</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>

                        <Form.Group controlId="productName">
                            <Form.Label>Name</Form.Label>
                            <Form.Control 
                            type="text"
                            value = {name} 
                            onChange = {event => setName(event.target.value)}
                            required/>
                        </Form.Group>

                        <Form.Group controlId="productDescription">
                            <Form.Label>Description</Form.Label>
                            <Form.Control 
                            type="text" 
                            value = {description} 
                            onChange = {event => setDescription(event.target.value)}
                            required/>
                        </Form.Group>

                        <Form.Group controlId="productPrice">
                            <Form.Label>Price</Form.Label>
                            <Form.Control 
                            type="number" 
                            value = {price} 
                            onChange={event => {
                                const newPrice = parseInt(event.target.value);
                                if (!isNaN(newPrice) && newPrice >= 1) {
                                    setPrice(newPrice);
                                } else {
                                    setPrice(1);
                                }
                            }}
                            required
                            inputMode="numeric"
                            />
                        </Form.Group>

                    </Modal.Body>

                    <Modal.Footer>
                        <Button variant='secondary' onClick = {closeEditModal}>Close</Button>
                        <Button variant='success' type='submit'>Submit</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    )
}