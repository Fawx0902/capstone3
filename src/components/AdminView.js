import { Table } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import AddProduct from './AddProduct'
import EditProduct from './EditProduct';
import ArchiveProduct from './ArchiveProduct';

export default function AdminView({productsData, fetchProducts}) {
    const [products, setProducts] = useState([]);

    useEffect(() => {
        const products_array = productsData.map(product => {
            return(
                <tr key = {product._id}>
                    <td>{product._id}</td>
                    <td>{product.name}</td>
                    <td>{product.description}</td>
                    <td>{product.price}</td>
                    <td>{product.isActive ? 'Available' : 'Unavailable'}</td>
                    <td>
                        <EditProduct product_id = {product._id} product_name = {product.name} fetchProducts = {fetchProducts}/>
                    </td>
                    <td>
                        <ArchiveProduct product_id = {product._id} product_name = {product.name} fetchProducts = {fetchProducts} isActive = {product.isActive}/>
                    </td>
                </tr>
            )
        })

        setProducts(products_array)
    }, [productsData, fetchProducts])

    return (
        <>
            <AddProduct fetchProducts = {fetchProducts}/>

            <Table striped bordered hover responsive className='table-format'>
                <thead>
                    <tr>
                        <th>Product ID</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th>Availability</th>
                        <th colspan = "2">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    { products }
                </tbody>
            </Table>
        </>
    )
}