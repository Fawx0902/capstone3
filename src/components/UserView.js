import { useState, useEffect } from 'react';
import ProductCard from './ProductCard';
import { Container, Row, Col } from 'react-bootstrap';

export default function UserView({ productsData }) {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    const activeProducts = productsData
      .filter(product => product.isActive === true)
      .map(product => (
        <Col key={product._id} xs={12} md={4} className="mx-auto">
          <ProductCard product={product} />
        </Col>
      ));

    setProducts(activeProducts);
  }, [productsData]);

  return (
    <Container>
      <Row>{products}</Row>
    </Container>
  );
}
