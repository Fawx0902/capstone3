import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveProduct({product_id, product_name, fetchProducts, isActive}) {
    const archiveProduct = (productId) => {
        fetch(`${process.env.REACT_APP_API_URL}/api/products/${productId}/archive`, {
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                isActive: isActive
            })
        }).then(response => response.json())
        .then(result => {
            if(result) {
                Swal.fire({
                    title: "Product Archived!",
                    text: `Product ${product_name} successfully archived.`,
                    icon: 'success'
                })
                fetchProducts();
            } else {
                Swal.fire({
                    title: "Something went wrong.",
                    text: "Please try again.",
                    icon: 'error'
                })
                fetchProducts();
            }
        })

    }

    const activateProduct = (productId) => {
        fetch(`${process.env.REACT_APP_API_URL}/api/products/${productId}/activate`, {
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                isActive: isActive
            })
        }).then(response => response.json())
        .then(result => {
            if(result) {
                Swal.fire({
                    title: "Product Activated!",
                    text: `Product ${product_name} successfully activated.`,
                    icon: 'success'
                })
                fetchProducts();
            } else {
                Swal.fire({
                    title: "Something went wrong.",
                    text: "Please try again.",
                    icon: 'error'
                })
                fetchProducts();
            }
        })
    }

    return (
        <>
            {
                (isActive) ?
                    <Button variant = "warning" size = "sm" onClick = {() => archiveProduct(product_id)}>Archive</Button>
                :
                    <Button variant = "success" size = "sm" onClick = {() => activateProduct(product_id)}>Activate</Button>
            }
        </>
    )
    
}