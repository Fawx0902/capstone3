import {Button, Row, Col} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner(){
    return(
        <Row>
            <Col className="p-5 text-center">
                <h1>Elektro Knicks</h1>
                <p>Embrace the Future, Cherish the Past!</p>
                <Button variant="primary" as = {Link} to = '/products'>Buy Now!</Button>
            </Col>
        </Row>
    )
}