import { useEffect, useState, useContext } from 'react';
import AdminView from '../components/AdminView';
import UserView from '../components/UserView';
import UserContext from '../UserContext';
import LoadingScreen from '../components/LoadingScreen';

export default function Products(){
    const {user} = useContext(UserContext)

    const [products, setProducts] = useState([])
    const [isLoading, setIsLoading] = useState(true);

    const fetchProducts = () => {
        fetch(`${process.env.REACT_APP_API_URL}/api/products/all`)
        .then(response => response.json())
        .then(result => {
            setProducts(result);
            setIsLoading(false);
        })
    }

    useEffect(() => {
        fetchProducts()
    }, [])

    return (
        <>
            {
                (user.isAdmin === true) ? 
                    <h1 className='text-center mt-4'>Product Dashboard</h1>
                :
                    <h1 className="text-center my-4">Products</h1>
            }

            {
                isLoading ? ( 
                <LoadingScreen />
            ) : (
                (user.isAdmin === true) ?
                    <AdminView productsData={products} fetchProducts = {fetchProducts}/>
                :
                    <UserView productsData={products}/> 
            )}
        </>
    )
}