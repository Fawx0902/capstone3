import { Card, Form, Button, Col, Row } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Register(){
    const {user} = useContext(UserContext)

    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [mobileNo, setMobileNo] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [isActive, setIsActive] = useState("false");

    // The useEffect arrow function will trigger everytime there are changes in the data within the 2nd argument array
    // Note: If the 2nd argument array is empty, then the function will only run 
    useEffect(() => {
        // Insert Effect Presto
        if((firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password !== "" && confirmPassword !== "") && (password === confirmPassword) && (mobileNo.length === 11)) {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [firstName, lastName, email, mobileNo, password, confirmPassword]);

    function registerUser(event){
        //Prevents page load upon form submission
        event.preventDefault();

        // Sends a request to the /register endpoint which will include all the fields necessary for that route.
        fetch(`${process.env.REACT_APP_API_URL}/api/users/register`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                mobileNo: mobileNo,
                password: password
            })
        }).then(response => response.json()).then(result => {
            if(result.message === 'Successfully registered a user!'){
                // Reset all input fields
                setFirstName("")
                setLastName("")
                setEmail("")
                setMobileNo("")
                setPassword("")
                setConfirmPassword("")

                Swal.fire({
					title: 'Register Success',
					text: 'Successfully registered an account!',
					icon: 'success'
				})
            } else {
                Swal.fire({
					title: 'Something went wrong.',
					text: result.message,
					icon: 'warning'
				})
            }
        })
    }

    return(
        (user.id !== null) ?
			<Navigate to='/products'/>
		:
            <>
            <h1 className="my-4 text-center">Register</h1>
                <Row className = "pb-4">
                    <Col md = {6} className = "mx-auto">
                        <Card className = "card-color">
                            <Card.Body>
                                <Form onSubmit={(event) => registerUser(event)}>
                                <Form.Group className="my-3">
                                    <Form.Label className="bold-text">First Name:</Form.Label>
                                    <Form.Control 
                                    type="text" 
                                    placeholder="Enter First Name" 
                                    required value={firstName} 
                                    onChange={event => 
                                    {setFirstName(event.target.value)}}
                                    />

                                </Form.Group>
                                <Form.Group className="my-3">
                                    <Form.Label className="bold-text">Last Name:</Form.Label>
                                    <Form.Control 
                                    type="text" 
                                    placeholder="Enter Last Name" 
                                    required value={lastName} 
                                    onChange={event => 
                                    {setLastName(event.target.value)}}
                                    />

                                </Form.Group>
                                <Form.Group className="my-3">
                                    <Form.Label className="bold-text">Email:</Form.Label>
                                    <Form.Control 
                                    type="email" 
                                    placeholder="Enter Email" 
                                    required value={email} 
                                    onChange={event => 
                                    {setEmail(event.target.value)}}         
                                    />

                                </Form.Group>
                                <Form.Group className="my-3">
                                    <Form.Label className="bold-text">Mobile No:</Form.Label>
                                    <Form.Control 
                                    type="number" 
                                    placeholder="Ex. 09729182643" 
                                    required value={mobileNo} 
                                    onChange={event => 
                                    {setMobileNo(event.target.value)}}
                                    />

                                </Form.Group>
                                <Form.Group className="my-3">
                                    <Form.Label className="bold-text">Password:</Form.Label>
                                    <Form.Control 
                                    type="password" 
                                    placeholder="Enter Password" 
                                    required value={password} 
                                    onChange={event => 
                                    {setPassword(event.target.value)}}
                                    />

                                </Form.Group>
                                <Form.Group className="my-3">
                                    <Form.Label className="bold-text">Confirm Password:</Form.Label>
                                    <Form.Control 
                                    type="password" 
                                    placeholder="Confirm Password" 
                                    required 
                                    value={confirmPassword} 
                                    onChange={event => 
                                    {setConfirmPassword(event.target.value)}}
                                    />
                                </Form.Group>
                                <div className = "text-center">
                                    <Button className = "my-1" variant="primary" type="submit" disabled={isActive === false}>Register</Button>  
                                </div>            
                                </Form>
                                <div className="text-center mt-1">
                                    <p>
                                        <span className='light-bold-text'>
                                            Already have an account? 
                                        </span>{' '}
                                        <Link to="/login" className = "link-no-underline">Click Here!</Link>
                                    </p>
                                </div>
                            </Card.Body>
                    </Card>
                </Col>
            </Row>
        </>
    )
}