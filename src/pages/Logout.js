import { Navigate } from 'react-router-dom';
import { useContext, useEffect } from 'react';
import UserContext from '../UserContext'

export default function Logout() {
    const { unsetUser, setUser } = useContext(UserContext);

    unsetUser();

    // Clears the token from the global user state
	useEffect(() => {
		setUser({
			id: null,
			isAdmin: null
		});
	}, [setUser]);

    return(
        <Navigate to = '/login'/>
    )
}