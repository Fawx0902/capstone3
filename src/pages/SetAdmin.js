import { Table, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import LoadingScreen from '../components/LoadingScreen';
import Swal from 'sweetalert2';

export default function SetAdmin() {
    const { user } = useContext(UserContext);

    const [users, setUsers] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    const fetchUsers = () => {
        fetch(`${process.env.REACT_APP_API_URL}/api/users/alldetails`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
        .then(result => {
            setUsers(result);
            setIsLoading(false);
        });
    };

    useEffect(() => {
        fetchUsers();
    }, []);

    const makeAdmin = (userId, firstName, lastName) => {
        // Implement the logic to set a user as admin here.
        fetch(`${process.env.REACT_APP_API_URL}/api/users/setadmin`, {
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id: userId,
                firstName: firstName,
                lastName: lastName
            })
        })
        .then(response => response.json())
        .then(result => {
            if (result) {
                Swal.fire({
                    title: "User granted Admin!",
                    text: result.message,
                    icon: 'success'
                })
                fetchUsers();
            } else {
                Swal.fire({
                    title: "Something went wrong.",
                    text: 'An error occured while trying to grant a user Admin, please try again later.',
                    icon: 'error'
                })
                fetchUsers();
            }
        })
    };

    return (
        <>
            <h1 className='text-center my-4'>User Dashboard</h1>
            {
                user.isAdmin === true ?
                isLoading ? (
                    <LoadingScreen/>
                ) : (
                <>
                    <Table striped bordered hover responsive className = "table-format">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Mobile Number</th>
                                <th>User Status</th>
                                <th>Set Admin</th>
                            </tr>
                        </thead>
                        <tbody>
                            {Array.isArray(users) && users.length > 0 ? (
                                users.map(userData => (
                                    <tr key={userData._id}>
                                        <td>{userData._id}</td>
                                        <td>{userData.firstName} {userData.lastName}</td>
                                        <td>{userData.email}</td>
                                        <td>{userData.mobileNo}</td>
                                        <td>
                                            {
                                                userData.isAdmin === true ? 
                                                <p>Admin</p> 
                                                : 
                                                <p>Consumer</p>
                                            }
                                        </td>
                                        <td>
                                            {
                                                userData.isAdmin === true ? 
                                                <p>Already Admin</p>
                                                : 
                                                <Button variant = "warning" onClick = {() => makeAdmin(userData._id, userData.firstName, userData.lastName)}>
                                                Set Admin
                                                </Button>
                                            }
                                        </td>
                                    </tr>
                                ))
                            ) : (
                                <tr>
                                    <td colSpan="6">No users found</td>
                                </tr>
                            )}
                        </tbody>
                    </Table>
                </>
            ) : (
                <Navigate to='/' />
            )}
        </>
    );
}