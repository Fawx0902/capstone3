import { useEffect, useState, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import UserOrder from '../components/UserOrder';
import AdminOrder from '../components/AdminOrder';
import UserContext from '../UserContext';
import LoadingScreen from '../components/LoadingScreen';

export default function Products(){
    const {user} = useContext(UserContext)

    const [orders, setOrders] = useState([])
    const [isLoading, setIsLoading] = useState(true);

    const fetchAllOrders = () => {
        fetch(`${process.env.REACT_APP_API_URL}/api/orders/all`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
        .then(result => {
            setOrders(result);
            setIsLoading(false);
        });
    }

    const fetchOrders = () => {
        fetch(`${process.env.REACT_APP_API_URL}/api/orders/`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
        .then(result => {
            setOrders(result);
            setIsLoading(false);
        });
    }

    useEffect(() => {
        if (user.isAdmin === true) {
            fetchAllOrders();
        } else {
            fetchOrders();
        }
    }, [user.isAdmin])

    return (
        <>
            <h1 className='text-center my-4'>Order History</h1>
            {
                user.id === null ? (
                    <Navigate to='/' />
                ) : isLoading ? (
                    <LoadingScreen />
                ) : user.isAdmin ? (
                    <AdminOrder ordersData={orders} />
                ) : (
                    <UserOrder ordersData={orders} userId={user.id} />
                )
            }
        </>
    )
}