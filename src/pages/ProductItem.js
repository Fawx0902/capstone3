import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function ProductItem(){
    const {productId} = useParams();
    const {user} = useContext(UserContext);

    const navigate = useNavigate();

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    const [quantity, setQuantity] = useState(1);

    const checkout = (event, id) => {
        event.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/api/orders/checkout`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                productId: id,
                price: price,
                quantity: quantity
            })
        })
        .then(response => response.json())
        .then(result => {
            if(result.message === 'Successfully checkout Order!'){
                Swal.fire({
                    title: "Successfully Checkout Order!",
                    text: `Successfully bought ${name}!`,
                    icon: 'success'
                })
                
                navigate('/products')
            } else {
                Swal.fire({
                    title: "Something went wrong..",
                    text: result.message,
                    icon: 'warning'
                })
            }
        })
    }

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/api/products/${productId}`)
        .then(response => response.json())
        .then(result => {
            setName(result.name)
            setDescription(result.description)
            setPrice(result.price)
        })
    }, [productId])

    return (
        <>
            <Container>
                <Row>
                    <Col>
                    <Button variant = "warning" className = "my-4" as = {Link} to = "/products">🡰 Go back</Button>
                        <Card className = "card-color">
                            <Card.Body className="text-center">
                                <Card.Title><h1>{name}</h1></Card.Title>

                                <Card.Subtitle className = "m-1">Description:</Card.Subtitle>
                                <Card.Text>{description}</Card.Text>

                                <Card.Subtitle className = "m-1">Price:</Card.Subtitle>
                                <Card.Text>PHP {price}</Card.Text>

                                <Form onSubmit = {event => checkout(event, productId)}>
                                    <Card.Subtitle>Quantity:</Card.Subtitle>
                                    <Form.Group controlId = "productQuantity" className="mt-2 mx-auto quantity_input">
                                    <Form.Control 
                                        type="number" 
                                        value = {quantity} 
                                        onChange={event => {
                                            const newQuantity = parseInt(event.target.value);
                                            if (!isNaN(newQuantity) && newQuantity >= 1) {
                                                setQuantity(newQuantity);
                                            } else {
                                                setQuantity(1);
                                            }
                                        }}
                                        required
                                        inputMode="numeric"
                                        />
                                    </Form.Group>

                                    {
                                        (user.id !== null) ?
                                        <Button variant = "primary" type = "submit" className = "mt-3">Checkout</Button>
                                        :
                                        <Link className = "btn btn-danger btn-block mt-3" to = '/login'>Login to Buy Product</Link>
                                    }
                                </Form>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </>
    )
}